function features = make_table(y, original_sample_rate)

    [y, original_sample_rate] = audioread('viva.mp3');

    y = mean(y,2);
    y = y - mean(y);
    new_sample_rate = 8000;
    y = resample(y, new_sample_rate, original_sample_rate);

    window = 0.064 * new_sample_rate;
    noverlap = 0.032 * new_sample_rate;
    nfft = window;
    [S,F,T] = spectrogram(y, window, noverlap, nfft, new_sample_rate);

    %ne log magnituda spektograma
    figure;
    subplot(1,2,1);
    imagesc(F,T,real(S));
    title('ne log magnitude');
    xlabel('Frequency(Hz)');
    ylabel('Time(seconds)');

    %Log Magnituda spektograma
    subplot(1, 2, 2);
    imagesc(F,T,real(log(S)));
    title('Log Magnitude Spektrograma');
    xlabel('Frequency(Hz)');
    ylabel('Time(seconds)');

    %Lokalni vrhovi
    S = real(log(S));
    gs = 9;
    peaks = zeros('like',S);

    for shiftx=int32(-gs/2):1:int32(gs/2)
        for shifty=int32(-gs/2):1:int32(gs/2)
            CS = circshift(S,[shifty,shiftx]);
            peaks = peaks + ((S-CS)>0);
        end
    end

    P = (peaks - max(peaks(:))>=0);
    figure();
    imagesc(T,F,P);
    colormap(1-gray);
    title('Pre-Threshold Constellation');
    a = sum(P(:))/T(end);

    %Threshold peaks
    P = threshold_peaks(S,T,P,20);
    figure();
    imagesc(T,F,P)
    colormap(1-gray)
    title('Post-Threshold Constellation');

    figure();
    imagesc(T,F,P)
    colormap(1-gray)
    title('Constellation');
    dt_l = 0.05;
    dt_u = 0.4;
    df = 200;
    FANOUT = 3;
    [I,J] = find(P);
    features = [];

    %Iteracije kroz sve indekse i pronalazak parova
    for ii = 1:length(I)
        t1_index = J(ii);
        f1_index = I(ii);
        t1 = T(t1_index);
        f1 = F(f1_index);
        counter = 0;
        single_peak_features = [];
        for jj = 1:length(I)
            t2_index = J(jj);
            f2_index = I(jj);
            t2 = T(t2_index);
            f2 = F(f2_index);
            if t2 > t1+dt_l && t2 < t1 + dt_u && f2 > f1-df && f2 < f1+df
                single_peak_features = [ single_peak_features; [f1 f2 t1 t2-t1 t2] ];
            end
        end

        if ~isempty(single_peak_features)
            sortrows(single_peak_features, 4);

            if size(single_peak_features,1) > FANOUT
                single_peak_features = single_peak_features(1:FANOUT,:);
            end

            features = [ features; single_peak_features(:,1:4) ];

            for jj = 1:size(single_peak_features,1)
                feature = single_peak_features(jj,:);
                line( [feature(3), feature(5)], [feature(1), feature(2)]);
            end
        end
    end
end